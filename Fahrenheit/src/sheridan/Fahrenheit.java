package sheridan;

public class Fahrenheit {
	
	public static int convertFromCelsius(int celsius)
	{
		int fahrenheit = (int) Math.ceil((celsius * 9/5.0) + 32);
		
		return fahrenheit;
		
	}

	public static void main(String [] args)
	{
		int result = convertFromCelsius(14);
	
		System.out.println(result);
	}
}
