package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testConvertFromCelsius() {
		int result = Fahrenheit.convertFromCelsius(32);
		assertTrue("Temperature Calculated", result == 90);
	}
	
	@Test
	public void testConvertFromCelsiusException() {
		int result = Fahrenheit.convertFromCelsius(4);
		assertFalse("Temperature Calculated", result == 39);
	}
	
	@Test
	public void testConvertFromCelsiusBoundaryIn() {
		int result = Fahrenheit.convertFromCelsius(12);
		assertTrue("Temperature Calculated", result == 54);
	}
	

	@Test
	public void testConvertFromCelsiusBoundaryOut() {
		int result = Fahrenheit.convertFromCelsius(13);
		assertFalse("Temperature Calculated", result == 55);
	}
	

}
